import numpy as np
import pandas as pd
from sklearn import linear_model as sklearn_linear_model, \
    metrics as sklearn_metrics, \
    preprocessing as sklearn_preprocessing

TEST_CATEGORIES = "TEST_CATEGORIES"
TEST_DATA = "TEST_DATA"
TEST_FEATURES = "TEST_FEATURES"

TRAINING_CATEGORIES = "TRAINING_CATEGORIES"
TRAINING_DATA = "TRAINING_DATA"
TRAINING_FEATURES = "TRAINING_FEATURES"


def fetch_data_from_csvs() -> dict:
    """
    Fetch and parse training and test data from separate CSV files using Pandas
    :return: a dictionary containing training and test data, each as matrices
    """
    training_data = pd.read_csv("training-data.csv", header=None).values
    test_data = pd.read_csv("test-data.csv", header=None).values

    return {
        TRAINING_DATA: training_data,
        TEST_DATA: test_data,
    }


def separate_features_and_categories(training_and_test_data: dict) -> dict:
    """
    Separate features and categories from the training and test data sets
    :param training_and_test_data: dictionary containing training and test data matrices
    :return: a new dictionary separating out features from categories in each matrix
    """
    return {
        TRAINING_FEATURES: training_and_test_data[TRAINING_DATA][:, :-1].astype(np.float64),
        TRAINING_CATEGORIES: np.ravel(training_and_test_data[TRAINING_DATA][:, -1:]),
        TEST_FEATURES: training_and_test_data[TEST_DATA][:, :-1].astype(np.float64),
        TEST_CATEGORIES: np.ravel(training_and_test_data[TEST_DATA][:, -1:]),
    }


def scale_data(features_and_categories: dict) -> dict:
    """
    Scale features in both training and test data
    :param features_and_categories: dictionary containing features and categories for both training and testing
    :return: the same dictionary, with features scaled based on training features
    """
    scaler = sklearn_preprocessing.StandardScaler()
    scaler.fit(features_and_categories[TRAINING_FEATURES])

    features_and_categories[TRAINING_FEATURES] = scaler.transform(features_and_categories[TRAINING_FEATURES])
    features_and_categories[TEST_FEATURES] = scaler.transform(features_and_categories[TEST_FEATURES])

    return features_and_categories


def classify_test_samples(features_and_categories: dict) -> list:
    """
    Predict categories for test features based on the training data using a SGD Classifier
    :param features_and_categories: dictionary containing features and categories for both training and testing
    :return: a list of predicted categories for test features
    """
    clf = sklearn_linear_model.stochastic_gradient.SGDClassifier(max_iter=1000, tol=1e-3)

    clf.fit(features_and_categories[TRAINING_FEATURES], features_and_categories[TRAINING_CATEGORIES])
    predicted_categories = clf.predict(features_and_categories[TEST_FEATURES])

    return predicted_categories


def gather_classification_metrics(actual_categories: list, predicted_categories: list) -> None:
    """
    Collect metrics between the predicted and actual categories for test features, including accuracy rate
    :param actual_categories: a list containing the correct category for each test feature
    :param predicted_categories: a list containing the predicted category for each test feature
    :return: nothing (printing metrics)
    """
    accuracy = sklearn_metrics.accuracy_score(actual_categories, predicted_categories)
    metrics_report = sklearn_metrics.classification_report(actual_categories, predicted_categories)

    print(f"Accuracy rate: {round(accuracy, 2)}\n{metrics_report}")


def main():
    training_and_test_data = fetch_data_from_csvs()
    features_and_categories = separate_features_and_categories(training_and_test_data)
    features_and_categories = scale_data(features_and_categories)
    predicted_categories = classify_test_samples(features_and_categories)
    gather_classification_metrics(features_and_categories[TEST_CATEGORIES], predicted_categories)


if __name__ == "__main__":
    main()
